<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();

            $table->string('room_id')->comment('Mã phòng, khóa ngoại');
            $table->unsignedBigInteger('service_id')->nullable()->comment('Mã dịch vụ, khóa ngoại');

            $table->text('contact_name')->comment('Họ tên');
            $table->tinyInteger('sex')->default(1)->comment('Giới tính 0 = nữ, 1 = nam');
            $table->text('description')->comment('Mô tả thêm');
            $table->text('contact_mobile')->comment('Số điện thoại');
            $table->string('identity_number')->comment('Thẻ căn cước, cmnd');
            $table->text('contact_address')->comment('Địa chỉ');
            $table->bigInteger('total_price')->comment('Tổng giá');
            $table->bigInteger('prepay_price')->comment('Trả trước');
            $table->bigInteger('lack_price')->comment('Còn lại');
            $table->date('birthday')->comment('Ngày sinh');
            $table->date('order_date')->comment('Ngày đặt phòng');
            $table->date('pay_date')->comment('Ngày trả phòng');

            $table->index(['service_id']);

            $table->foreign('service_id')
                ->references('id')->on('services')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
