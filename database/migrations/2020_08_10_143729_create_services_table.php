<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();

            $table->string('name')->unique()->comment('Tên đồ ăn');
            $table->string('type_service')->unique()->comment('Loại đồ ăn');
            $table->double('price')->default(0)->comment('Đơn giá');
            $table->string('thumbnails')->default('')->comment('Hình đại diện');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
