<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();

            $table->string('code_number_staff')->comment('Số hiệu nhân viên, khóa ngoại');

            $table->string('username')->unique()->comment('Tên tài khoản');
            $table->string('password')->comment('Mật khẩu, đã băm bằng bcrypt');

            // Quyền hạn
            $table->string('role')->default('author');
            $table->boolean('status')->default(true)->comment('Tình trạng khóa tài khoản');

            $table->rememberToken();

            $table->index('code_number_staff');

            $table->foreign('code_number_staff')
                ->references('code_number_staff')->on('staff')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
