<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->id();

            $table->string('code_number_staff')->unique()->comment('Số hiệu nhân viên');

            $table->string('position_staff')->nullable()->comment('Chức vụ');

            $table->string('full_name')->comment('Họ tên nhân viên');
            $table->string('identity_number')->comment('Thẻ căn cước, cmnd');

            $table->string('home_town')->comment('Quê quán');
            $table->string('address')->comment('Địa chỉ liên hệ');

            $table->date('birthday')->comment('Ngày sinh');
            $table->unsignedInteger('sex')->comment('Giới tính: 0=Nữ, 1=Nam');

            $table->longText('experience')->comment('Kinh nghiệm');

            $table->string('folk')->comment('Dân tộc');
            $table->string('religion')->comment('Tôn giáo');

            $table->timestamp('pending_remove')->nullable()->comment('Thời gian xóa vĩnh viễn');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
