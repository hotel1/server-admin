<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();

            $table->string('code_number_room')->comment('Mã phòng');
            $table->unsignedBigInteger('floor_id')->comment('Mã tầng, khóa ngoại');

            // Thông tin cơ bản
            $table->string('slug', 200)->comment('Đường dẫn thân thiện, SEO URL');
            $table->string('name')->default('')->comment('Tên phòng');
            $table->double('price')->default(0)->comment('Giá phòng');
            $table->double('prepay_price')->default(0)->comment('Trả trước %');
            $table->unsignedInteger('room_type')->comment('Loại phòng: 1: Đơn, 2: đôi, 3: VIP đơn, 4: VIP đôi, 5: tập thể');

            $table->longText('description')->comment('Mô tả phòng');

            // Hình ảnh, video và vị trí
            $table->boolean('has_image')->default(false)->comment('Bài đăng có hình');
            $table->boolean('has_image_360')->default(false)->comment('Bài đăng có hình 360');
            $table->string('thumbnails')->default('')->comment('Hình đại diện');
            $table->string('youtube')->nullable()->default('')->comment('Link video trên youtube');

            // Chi tiết
            $table->unsignedInteger('area')->comment('Diện tích, 0 = N/a');
            $table->string('area_unit')->default('m2')->comment('Đơn vị diện tích');
            $table->string('address')->comment('Địa chỉ');

            // Thông tin khác
            $table->unsignedTinyInteger('main_direction')->default(0)->comment('Hướng phòng');
            $table->unsignedTinyInteger('nbr_bed_room')->default(0)->comment('Số giường');
            $table->unsignedTinyInteger('nbr_rest_room')->default(0)->comment('Số phòng về sinh');

            // Trạng thái
            $table->unsignedBigInteger('status')->comment('1: đã đặt, 2: trống, 3: đang dọn phòng');

            // Thời gian tồn tại
            $table->date('start_date')->nullable()->comment('Ngày bắt đầu');
            $table->date('end_date')->nullable()->comment('Ngày kết thúc');

            $table->index(['floor_id']);

            $table->foreign('floor_id')
                ->references('id')->on('floors')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
