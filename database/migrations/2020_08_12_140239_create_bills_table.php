<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('order_id')->comment('Mã đặt phòng, khóa ngoại');
            $table->unsignedBigInteger('order_service_id')->nullable()->comment('Mã dịch vụ đã đặt, khóa ngoại');

            $table->bigInteger('total_price')->comment('Tổng giá');
//            $table->tinyInteger('status')->default(0)->comment('Trạng thái đơn hàng 0 = đang xử lý, 1 = đã duyệt');

            // Tài khoản thanh toán (nhân viên)
            $table->unsignedBigInteger('user_id')->nullable()->comment('Tài khoản thanh toán, khóa ngoại');
            // Thời gian thanh toán
            $table->date('pay_date')->nullable()->comment('Ngày thanh toán');
            // Trạng thái
            $table->unsignedBigInteger('status')->nullable()->comment('1: thanh toán, 2: chưa thanh toán, 3: Chờ thanh toán');

            $table->index(['order_id']);
            $table->index(['order_service_id']);

            $table->foreign('order_id')
                ->references('id')->on('orders')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('order_service_id')
                ->references('id')->on('order_services')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
