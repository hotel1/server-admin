<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_services', function (Blueprint $table) {
            $table->id();

//            $table->string('room_id')->nullable()->comment('Mã phòng, khóa ngoại');

            $table->unsignedBigInteger('service_id')->nullable()->comment('Mã dịch vụ, khóa ngoại');

            $table->unsignedBigInteger('bill_id')->nullable()->comment('Mã hóa đơn, khóa ngoại');
            $table->bigInteger('order_count')->default(0)->comment('Số lượng');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_services');
    }
}
