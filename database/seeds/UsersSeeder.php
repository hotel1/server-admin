<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            // Thông tin đăng nhập
            'code_number_staff'   => 'NV001',
            'username'   => 'demoad',
            'password'   => bcrypt('123456'),

            // Quyền hạn
            'role'                       => 'admin',
            'status'                     => true,
        ]);

        User::create([
            // Thông tin đăng nhập
            'code_number_staff'   => 'NV002',
            'username'   => 'test',
            'password'   => bcrypt('123456'),

            // Quyền hạn
            'role'                       => 'author',
            'status'                     => true,

        ]);
    }
}
