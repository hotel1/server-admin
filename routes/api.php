<?php

use Illuminate\Contracts\Routing\Registrar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

// Kiểm tra đăng nhập
Route::group(['prefix' => 'auth'], function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::get('list', 'AuthController@index');
});

// check logout
Route::group([
    'prefix' => 'auth',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('logout', 'AuthController@logout');
        $router->get('me', 'AuthController@me');
    });
});

// Tài khoản người sử dụng
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix' => 'user',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'UserController@index');
        $router->post('create', 'UserController@create');
        $router->get('show/{id}', 'UserController@show');
        $router->post('update/{id}', 'UserController@update');
        $router->delete('remove/{id}', 'UserController@remove');
        $router->post('searchAll', 'UserController@searchAll');
//    });
});

// Floor
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix' => 'floor',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('getOption', 'FloorController@getOption');
        $router->get('list', 'FloorController@index');
        $router->post('create', 'FloorController@create');
        $router->get('show/{id}', 'FloorController@show');
        $router->post('update/{id}', 'FloorController@update');
        $router->delete('remove/{id}', 'FloorController@remove');
        $router->post('searchAll', 'FloorController@searchAll');
//    });
});

// Room
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix' => 'room',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'RoomController@index');
        $router->post('create', 'RoomController@create');
        $router->get('show/{id}', 'RoomController@show');
        $router->post('update/{id}', 'RoomController@update');
        $router->delete('remove/{id}', 'RoomController@remove');
        $router->post('searchAll', 'RoomController@searchAll');
        $router->post('upload', 'RoomController@upload');
//    });
});


// Service
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix' => 'service',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
    $router->get('getOption', 'ServiceController@getOption');
    $router->get('list', 'ServiceController@index');
    $router->post('create', 'ServiceController@create');
    $router->get('show/{id}', 'ServiceController@show');
    $router->post('update/{id}', 'ServiceController@update');
    $router->delete('remove/{id}', 'ServiceController@remove');
    $router->post('searchAll', 'ServiceController@searchAll');
    $router->post('upload', 'ServiceController@upload');
//    });
});

// Order
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix' => 'order',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'OrderController@index');
        $router->get('show/{id}', 'OrderController@show');
        $router->delete('remove/{id}', 'OrderController@remove');
        $router->post('searchAll', 'OrderController@searchAll');
//    });
});

// Order Service
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix' => 'orderService',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
    $router->get('list', 'OrderServiceController@index');
    $router->post('create', 'OrderServiceController@create');
    $router->get('show/{id}', 'OrderServiceController@show');
    $router->post('update/{id}', 'OrderServiceController@update');
    $router->delete('remove/{id}', 'OrderServiceController@remove');
    $router->post('searchAll', 'OrderServiceController@searchAll');
//    });
});

// Bill
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix' => 'bill',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
    $router->get('getOption', 'BillController@getOption');
    $router->get('list', 'BillController@index');
    $router->get('show/{id}', 'BillController@show');
    $router->delete('remove/{id}', 'BillController@remove');
    $router->post('searchAll', 'BillController@searchAll');
    $router->post('checkPay/{id}', 'BillController@checkPay');
//    });
});

// Staff
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix' => 'staff',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
    $router->get('list', 'StaffController@index');
    $router->post('create', 'StaffController@create');
    $router->get('show/{id}', 'StaffController@show');
    $router->post('update/{id}', 'StaffController@update');
    $router->delete('remove/{id}', 'StaffController@remove');
    $router->post('searchAll', 'StaffController@searchAll');
//    });
});

// IMAGE UPLOAD
Route::post('image.upload', 'ImageController@upload');