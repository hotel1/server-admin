<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'room_id',
        'contact_name',
        'sex',
        'description',
        'contact_mobile',
        'identity_number',
        'contact_address',
        'total_price',
        'prepay_price',
        'lack_price',
        'birthday',
        'order_date',
        'pay_date',
    ];

    protected $filter = [
        'id',
        'room_id',
        'contact_name',
        'sex',
        'description',
        'contact_mobile',
        'identity_number',
        'contact_address',
        'total_price',
        'prepay_price',
        'lack_price',
        'birthday',
        'order_date',
        'pay_date',
    ];
}
