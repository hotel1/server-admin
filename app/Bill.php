<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'order_id',
        'order_service_id',
        'total_price',
    ];

    protected $filter = [
        'id',
        'order_id',
        'order_service_id',
        'total_price',
    ];

    public function orders()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function services()
    {
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }
}
