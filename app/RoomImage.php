<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomImage extends Model
{
    protected $fillable = [
        'room_id',
        'filepath',
        'type',
        'o',
        'xs',
        'uploaded_data',
        'sort_order',
    ];

    protected $casts = [
        'uploaded_data' => 'array',
    ];
}
