<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'name',
        'type_service',
        'price',
        'thumbnails'
    ];

    protected $filter = [
        'id',
        'name',
        'type_service',
        'price',
        'thumbnails'
    ];
}
