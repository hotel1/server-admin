<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'code_number_staff',
        'position_staff',
        'full_name',
        'identity_number',
        'home_town',
        'address',
        'birthday',
        'sex',
        'experience',
        'folk',
        'religion'
    ];

    protected $filter = [
        'id',
        'code_number_staff',
        'position_staff',
        'full_name',
        'identity_number',
        'home_town',
        'address',
        'birthday',
        'sex',
        'experience',
        'folk',
        'religion'
    ];
}
