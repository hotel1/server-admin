<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderServiceCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service_id'                                          => 'required',
            'bill_id'                                             => 'required',
            'order_count'                                         => 'required',
        ];
    }

    public function messages()
    {
        return [
            'service_id.required'                                   => 'Bạn chưa nhập dịch vụ',
            'bill_id.required'                                      => 'Bạn chưa nhập hóa đơn',
            'order_count.required'                                  => 'Bạn chưa nhập số lượng',
        ];
    }
}