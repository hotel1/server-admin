<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                                          => 'required',
            'type_service'                                  => 'required',
            'price'                                         => 'required',
            'thumbnails'                                    => 'required',
        ];
    }

    public function messages()
    {
        return [
            'type_service.required'                         => 'Bạn chưa nhập loại dịch vụ',
            'name.required'                                 => 'Bạn chưa nhập tên dịch vụ',
            'price.required'                                => 'Bạn chưa nhập giá',
            'thumbnails.required'                           => 'Bạn chưa nhập hình ảnh',
        ];
    }
}