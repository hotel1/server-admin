<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoomCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code_number_room'                                  => 'required',
            'floor_id'                                          => 'required',
            'name'                                              => 'required',
            'price'                                             => 'required',
            'prepay_price'                                      => 'required',
            'description'                                       => 'required',
            'room_type'                                         => 'required',

            // Xóa ảnh cũ
            'imagesOld.*'                                       => 'required',
            // Thư viện ảnh
            'images.*.filepath'                                 => 'required',
            'images.*.type'                                     => 'required',
            'images.*.sort_order'                               => 'required|numeric',
            'images.*.uploaded_data.*.ranting'                  => 'required',
            'images.*.uploaded_data.*.filename'                 => 'required',
            'images.*.uploaded_data.*.width'                    => 'required|numeric',
            'images.*.uploaded_data.*.height'                   => 'required|numeric',

            'thumbnails'                                        => 'required',
            'youtube'                                           => 'required',
            'area'                                              => 'required',
            'area_unit'                                         => 'required',
            'address'                                           => 'required',
            'main_direction'                                    => 'required',
            'nbr_bed_room'                                      => 'required',
            'nbr_rest_room'                                     => 'required',
            'status'                                            => 'required',
            'start_date'                                        => 'required',
            'end_date'                                          => 'required',
        ];
    }

    public function messages()
    {
        return [
            'code_number_room.required'                         => 'Bạn chưa nhập mã phòng',
            'floor_id.required'                                 => 'Bạn chưa nhập mã tầng',
            'name.required'                                     => 'Bạn chưa nhập tên phòng',
            'price.required'                                    => 'Bạn chưa nhập đơn giá',
            'prepay_price.required'                             => 'Bạn chưa nhập trả trước',
            'description.required'                              => 'Bạn chưa nhập mô tả',
            'room_type.required'                                => 'Bạn chưa nhập loại phòng',
            'thumbnails.required'                               => 'Bạn chưa nhập hình ảnh',
            'youtube.required'                                  => 'Bạn chưa nhập youtube',
            'area.required'                                     => 'Bạn chưa nhập diện tích',
            'area_unit.required'                                => 'Bạn chưa nhập đơn vị diện tích',
            'address.required'                                  => 'Bạn chưa nhập địa chỉ',
            'main_direction.required'                           => 'Bạn chưa nhập hướng phòng',
            'nbr_bed_room.required'                             => 'Bạn chưa nhập phòng ngủ',
            'nbr_rest_room.required'                            => 'Bạn chưa nhập phòng tắm',
            'status.required'                                   => 'Bạn chưa nhập trạng thái',
            'start_date.required'                               => 'Bạn chưa nhập ngày bắt đầu',
            'end_date.required'                                 => 'Bạn chưa nhập ngày kết thúc',
        ];
    }
}