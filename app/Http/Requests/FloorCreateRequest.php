<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FloorCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code_number_floor'                            => 'required',
            'name'                                         => 'required'
        ];
    }

    public function messages()
    {
        return [
            'code_number_floor.required'                   => 'Bạn chưa nhập mã tầng',
            'name.required'                                => 'Bạn chưa nhập tên tầng'
        ];
    }
}
