<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StaffCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code_number_staff'                                     => 'required',
            'position_staff'                                        => 'required',
            'full_name'                                             => 'required',
            'identity_number'                                       => 'required',
            'home_town'                                             => 'required',
            'address'                                               => 'required',
            'birthday'                                              => 'required',
            'sex'                                                   => 'required',
            'experience'                                            => 'required',
            'folk'                                                  => 'required',
            'religion'                                              => 'required',
        ];
    }

    public function messages()
    {
        return [
            'code_number_staff.required'                            => 'Bạn chưa nhập mã nhân viên',
            'position_staff.required'                               => 'Bạn chưa nhập chức vụ',
            'full_name.required'                                    => 'Bạn chưa nhập họ tên',
            'identity_number.required'                              => 'Bạn chưa nhập cmnd',
            'home_town.required'                                    => 'Bạn chưa nhập quê quán',
            'address.required'                                      => 'Bạn chưa nhập nơi ở hiện tại',
            'birthday.required'                                     => 'Bạn chưa nhập ngày sinh',
            'sex.required'                                          => 'Bạn chưa nhập giới tính',
            'experience.required'                                   => 'Bạn chưa nhập kinh nghiệm',
            'folk.required'                                         => 'Bạn chưa nhập dân tộc',
            'religion.required'                                     => 'Bạn chưa nhập tôn giáo',
        ];
    }
}