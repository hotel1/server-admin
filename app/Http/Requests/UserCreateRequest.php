<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code_number_staff'                                 => 'required',
            'username'                                          => 'required',
            'password'                                          => 'required',
            'role'                                              => 'required',
            'status'                                            => 'required',
        ];
    }

    public function messages()
    {
        return [
            'code_number_staff.required'                        => 'Bạn chưa nhập mã nhân viên',
            'username.required'                                 => 'Bạn chưa nhập tài khoản',
            'password.required'                                 => 'Bạn chưa nhập mật khẩu',
            'role.required'                                     => 'Bạn chưa nhập quyền hạn',
            'status.required'                                   => 'Bạn chưa nhập trạng thái',
        ];
    }
}