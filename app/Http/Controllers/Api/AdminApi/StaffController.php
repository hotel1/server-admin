<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\StaffCreateRequest;
use App\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StaffController extends AbstractApiController
{
    public function index(Request $request)
    {
        $staff = Staff::query()
            ->select([
                'id',
                'code_number_staff',
                'position_staff',
                'full_name',
                'identity_number',
                'home_town',
                'address',
                'birthday',
                'sex',
                'experience',
                'folk',
                'religion'
            ])
            ->DataTablePaginate($request);

        return $this->item($staff);
    }

    public function create(StaffCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        $payload['code_number_staff']                           = $validatedData['code_number_staff'];
        $payload['position_staff']                              = $validatedData['position_staff'];
        $payload['full_name']                                   = $validatedData['full_name'];
        $payload['identity_number']                             = $validatedData['identity_number'];
        $payload['home_town']                                   = $validatedData['home_town'];
        $payload['address']                                     = $validatedData['address'];
        $payload['birthday']                                    = $validatedData['birthday'];
        $payload['sex']                                         = $validatedData['sex'];
        $payload['experience']                                  = $validatedData['experience'];
        $payload['folk']                                        = $validatedData['folk'];
        $payload['religion']                                    = $validatedData['religion'];

        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['code_number_staff'])) {
            $this->setMessage('Đã tồn tại nhân viên');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $staff = Staff::create($payload);
        DB::beginTransaction();

        try {
            $staff->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm nhân viên thành công!');
            $this->setStatusCode(200);
            $this->setData($staff);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Staff::query()->findOrFail($id);
    }

    public function update(StaffCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $staff = Staff::query()->findOrFail($id);
        if (!$staff) {
            $this->setMessage('Không có nhân viên này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật nhân viên
                $staff->code_number_staff                           = $validatedData['code_number_staff'];
                $staff->position_staff                              = $validatedData['position_staff'];
                $staff->full_name                                   = $validatedData['full_name'];
                $staff->identity_number                             = $validatedData['identity_number'];
                $staff->home_town                                   = $validatedData['home_town'];
                $staff->address                                     = $validatedData['address'];
                $staff->birthday                                    = $validatedData['birthday'];
                $staff->sex                                         = $validatedData['sex'];
                $staff->experience                                  = $validatedData['experience'];
                $staff->folk                                        = $validatedData['folk'];
                $staff->religion                                    = $validatedData['religion'];

                $staff->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($staff);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Staff::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($code_number_staff)
    {
        $staff = Staff::query()->get();
        foreach ($staff->pluck('code_number_staff') as $item) {
            if ($code_number_staff == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $staff = Staff::query()
            ->select([
                'id',
                'code_number_staff',
                'position_staff',
                'full_name',
                'identity_number',
                'home_town',
                'address',
                'birthday',
                'sex',
                'experience',
                'folk',
                'religion'
            ])
            ->where('code_number_staff', 'LIKE', "%$search%")
            ->orWhere('full_name', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($staff);
    }
}
