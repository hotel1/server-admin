<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\ServiceCreateRequest;
use App\Service;
use Carbon\Carbon;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceController extends AbstractApiController
{
    public function getOption(Request $request)
    {
        $service = Service::query()->get();

        return $this->item($service);
    }

    public function index(Request $request)
    {
        $service = Service::query()
            ->select([
                'id',
                'name',
                'type_service',
                'price',
                'thumbnails'
            ])
            ->DataTablePaginate($request);

        return $this->item($service);
    }

    public function create(ServiceCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        $payload['name']                                = $validatedData['name'];
        $payload['type_service']                        = $validatedData['type_service'];
        $payload['price']                               = $validatedData['price'];
        $payload['thumbnails']                          = $validatedData['thumbnails'];

        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên dịch vụ');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $service = Service::create($payload);
        DB::beginTransaction();

        try {
            $service->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm dịch vụ thành công!');
            $this->setStatusCode(200);
            $this->setData($service);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Service::query()->findOrFail($id);
    }

    public function update(ServiceCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();

        $service = Service::query()->findOrFail($id);
        if (!$service) {
            $this->setMessage('Không có dịch vụ này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $service->name                              = $validatedData['name'];
                $service->type_service                      = $validatedData['type_service'];
                $service->price                             = $validatedData['price'];
                $service->thumbnails                        = $validatedData['thumbnails'];

                $service->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($service);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Service::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $service = Service::query()->get();
        foreach ($service->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $service = Service::query()
            ->select([
                'id',
                'name',
                'type_service',
                'price',
                'thumbnails'
            ])
            ->where('name', 'LIKE', "%$search%")
            ->orWhere('type_service', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($service);
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/service'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
