<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\RoomCreateRequest;
use App\RoomImage;
use App\Room;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class RoomController extends AbstractApiController
{
    public function index(Request $request)
    {
        $room = Room::query()
            ->select([
                'id',
                'code_number_room',
                'floor_id',
                'slug',
                'name',
                'price',
                'prepay_price',
                'description',
                'room_type',
                'has_image',
                'has_image_360',
                'thumbnails',
                'youtube',
                'area',
                'area_unit',
                'address',
                'main_direction',
                'nbr_bed_room',
                'nbr_rest_room',
                'status',
                'start_date',
                'end_date'
            ])
            ->DataTablePaginate($request);

        return $this->item($room);
    }

    public function create(RoomCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['code_number_room']                        = $validatedData['code_number_room'];
        $payload['floor_id']                                = $validatedData['floor_id'];
        $payload['name']                                    = $validatedData['name'];

        $payload['slug']                                    = $slugify->slugify($validatedData['name']);

        $payload['price']                                   = $validatedData['price'];
        $payload['prepay_price']                                   = $validatedData['prepay_price'];
        $payload['description']                             = $validatedData['description'];

        $payload['room_type']                               = $validatedData['room_type'];

        // Hình ảnh
        $payload['images']                                  = ! empty($validatedData['images']) ? $validatedData['images'] : [];

        $payload['thumbnails']                              = $validatedData['thumbnails'];
        $payload['youtube']                                 = $validatedData['youtube'];

        $payload['area']                                    = $validatedData['area'];
        $payload['area_unit']                               = $validatedData['area_unit'];
        $payload['address']                                 = $validatedData['address'];
        $payload['main_direction']                          = $validatedData['main_direction'];
        $payload['nbr_bed_room']                            = $validatedData['nbr_bed_room'];
        $payload['nbr_rest_room']                           = $validatedData['nbr_rest_room'];
        $payload['status']                                  = 2;

        $payload['start_date']                              = $validatedData['start_date'];
        $payload['end_date']                                = $validatedData['end_date'];

        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên phòng');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $room = Room::create($payload);
        DB::beginTransaction();

        if (! empty($payload['images'])) {

//                foreach ($payload['images'] as $image_idx => $image) {
//                    $currentBucketPath = $image['filepath'];
//
//                    foreach ($image['uploaded_data'] as $thumb_index => $targetFile) {
//                        Storage::disk('minio')->copy(
//                            $currentBucketPath.'/'.$targetFile['filename'],
//                            $newBucketPath.'/'.$targetFile['filename']
//                        );
//
//                        $payload['images'][$image_idx]['filepath'] = $newBucketPath;
//                    }
//                }

            // Cập nhật bucket path
            $room->addImages($payload['images']);
        }

        try {
            $room->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm phòng thành công!');
            $this->setStatusCode(200);
            $this->setData($room);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
//        return Post::query()->findOrFail($id);


//        $images = ProductImage::query()->where('post_id', '=', $id)->get();
//        $product = Post::query()->findOrFail($id);
//
//        $result = [
//            'images' => $images,
//            'post'     => $product,
//        ];
//
//        return $this->item($result);
        $query = Room::query();
        $query->where('id', '=', $id);
        $room = $query->firstOrFail();
        $room->load('images');

        return $this->item($room);
    }

    public function RemoveImagesOld($id)
    {
        return RoomImage::query()->where('id', '=', $id)->delete();
    }

    public function update(RoomCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $room = Room::query()->findOrFail($id);
        if (!$room) {
            $this->setMessage('Không có phòng này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $room->code_number_room                         = $validatedData['code_number_room'];
                $room->floor_id                                 = $validatedData['floor_id'];
                $room->name                                     = $validatedData['name'];
                $room->slug                                     = $slugify->slugify($validatedData['name']);
                $room->price                                    = $validatedData['price'];
                $room->prepay_price                                    = $validatedData['prepay_price'];
                $room->description                              = $validatedData['description'];
                $room->room_type                                = $validatedData['room_type'];

                $payload['images']                              = ! empty($validatedData['images']) ? $validatedData['images'] : [];
                $payload['imagesOld']                           = ! empty($validatedData['imagesOld']) ? $validatedData['imagesOld'] : [];

                $room->thumbnails                               = $validatedData['thumbnails'];
                $room->youtube                                  = $validatedData['youtube'];
                $room->area                                     = $validatedData['area'];
                $room->area_unit                                = $validatedData['area_unit'];
                $room->address                                  = $validatedData['address'];
                $room->main_direction                           = $validatedData['main_direction'];
                $room->nbr_bed_room                             = $validatedData['nbr_bed_room'];
                $room->nbr_rest_room                            = $validatedData['nbr_rest_room'];
                $room->status                                   = $validatedData['status'];

                $room->start_date                               = $validatedData['start_date'];
                $room->end_date                                 = $validatedData['end_date'];
                // Cập nhật sort order
//                $school->sort_order = $validatedData['sort_order'];

                // Tạo và lưu
//                $product = Product::create($payload);
//                DB::beginTransaction();

                // ƯU TIÊN CHẠY XÓA TRƯỚC ADD SAU
                if (! empty($payload['imagesOld'])) {
                    $room->removeImages($payload['imagesOld']);
                }

                if (! empty($payload['images'])) {
                    // Cập nhật bucket path
                    $room->addImages($payload['images']);
                }

                $room->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage($payload['imagesOld']);
                $this->setStatusCode(200);
                $this->setData($room);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Room::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $room = Room::query()->get();
        foreach ($room->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $room = Room::query()
            ->select([
                'id',
                'code_number_room',
                'floor_id',
                'slug',
                'name',
                'price',
                'prepay_price',
                'description',
                'room_type',
                'has_image',
                'has_image_360',
                'thumbnails',
                'youtube',
                'area',
                'area_unit',
                'address',
                'main_direction',
                'nbr_bed_room',
                'nbr_rest_room',
                'status',
                'start_date',
                'end_date'
            ])
            ->where('name', 'LIKE', "%$search%")
            ->orWhere('price', 'LIKE', "%$search%")
            ->orWhere('description', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($room);
    }
}
