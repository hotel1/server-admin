<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Bill;
use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\OrderServiceCreateRequest;
use App\OrderService;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderServiceController extends AbstractApiController
{
    public function index(Request $request)
    {
        $orderService = OrderService::query()
            ->select([
                'id',
                'service_id',
                'bill_id',
                'order_count',
                'created_at',
            ])
            ->with('services')
            ->DataTablePaginate($request);

        return $this->item($orderService);
    }

    public function create(OrderServiceCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        foreach ($request['service_id'] as $item) {
//            $payload['code_number']                     = $item['value'];
//            $payload['school_id']                       = $item['valueSchoolId'];

            $payload['service_id']                                = $item['id'];
            $payload['bill_id']                                   = $validatedData['bill_id'];
            $payload['order_count']                               = $validatedData['order_count'];

            // Tạo và lưu
            $orderService = OrderService::create($payload);
            DB::beginTransaction();

            // START tính tổng tiền bills sau khi thêm mới dịch vụ
            $service = Service::query()->findOrFail($orderService->service_id);
            $bill = Bill::query()->findOrFail($validatedData['bill_id']);

            $bill->total_price      = $bill->total_price + ($orderService->order_count * $service->price) ;

            $bill->save();
            DB::commit();
            // END

            try {
                $orderService->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Thêm dịch vụ thành công!');
                $this->setStatusCode(200);
                $this->setData($orderService);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }


        return $this->respond();
    }

    public function show($id)
    {
        return OrderService::query()->findOrFail($id);
    }

    public function update(OrderServiceCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $orderService = OrderService::query()->findOrFail($id);
        if (!$orderService) {
            $this->setMessage('Không có dịch vụ này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $orderService->service_id                              = $validatedData['service_id'];
                $orderService->bill_id                                 = $validatedData['bill_id'];
                $orderService->order_count                             = $validatedData['order_count'];

                $orderService->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($orderService);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        OrderService::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $orderService = OrderService::query()
            ->select([
                'id',
                'service_id',
                'bill_id',
                'order_count',
            ])
            ->where('service_id', 'LIKE', "%$search%")
            ->orWhere('bill_id', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($orderService);
    }
}
