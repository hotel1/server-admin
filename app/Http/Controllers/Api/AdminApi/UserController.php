<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\User;
use App\Http\Requests\UserCreateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends AbstractApiController
{
    public function index(Request $request)
    {
        $user = User::query()
            ->select([
                'id',
                'code_number_staff',
                'username',
                'password',
                'role',
                'status'
            ])
            ->DataTablePaginate($request);

        return $this->item($user);
    }

    public function create(UserCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        $payload['code_number_staff']                       = $validatedData['code_number_staff'];
        $payload['username']                                = $validatedData['username'];
        $payload['password']                                = $validatedData['password'];
        $payload['role']                                    = $validatedData['role'];
        $payload['status']                                  = $validatedData['status'];

        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['username'])) {
            $this->setMessage('Đã tồn tại tài khoản');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $user = User::create($payload);
        DB::beginTransaction();

        try {
            $user->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm tài khoản thành công!');
            $this->setStatusCode(200);
            $this->setData($user);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return User::query()->findOrFail($id);
    }

    public function update(UserCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $user = User::query()->findOrFail($id);
        if (!$user) {
            $this->setMessage('Không có tài khoản này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên tầng
                $user->code_number_staff                        = $validatedData['code_number_staff'];
                $user->username                                 = $validatedData['username'];
                $user->password                                 = $validatedData['password'];
                $user->role                                     = $validatedData['role'];
                $user->status                                   = $validatedData['status'];

                $user->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($user);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        User::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($title)
    {
        $user = User::query()->get();
        foreach ($user->pluck('username') as $item) {
            if ($title == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $user = User::query()
            ->select([
                'id',
                'code_number_staff',
                'username',
                'password',
                'role',
                'status'
            ])
            ->where('code_number_staff', 'LIKE', "%$search%")
            ->orWhere('username', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($user);
    }
}
