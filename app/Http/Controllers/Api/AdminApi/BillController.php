<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\StaffCreateRequest;
use App\Bill;
use App\OrderService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BillController extends AbstractApiController
{
    public function getOption(Request $request)
    {
        $bill = Bill::query()->get();

        return $this->item($bill);
    }

    public function index(Request $request)
    {
        $bill = Bill::query()
            ->select([
                'id',
                'order_id',
                'order_service_id',
                'total_price',
            ])
//            ->with('products')
            ->DataTablePaginate($request);

        return $this->item($bill);
    }

    public function show($id)
    {
        // Tìm kiếm hóa đơn cần xử lý
        $bill = Bill::query()->with('orders', 'services')->findOrFail($id);

        // Lấy tất cả dịch vụ thuộc hóa đơn
        $orderService = OrderService::query()
            ->with('services')
            ->where('bill_id', '=', $id)
            ->get();

        // Tổng tiền
        $totalPrice = 0;
        // Lấy mỗi dịch vụ của tất cả dịch vụ thuộc hóa đơn
        foreach ($orderService as $item)
        {
            $price = $item['services']['price'] * $item['order_count'];
            $totalPrice += $price;
        }

        return $this->item([$bill, $orderService, $totalPrice]);
    }

    public function checkPay($id)
    {
        $bill = Bill::query()->findOrFail($id);
        if (!$bill) {
            $this->setMessage('Không có hóa đơn này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $bill->user_id                              = Auth::id();
                $bill->pay_date                             = Carbon::now();
                $bill->status                               = 1;

                $bill->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($bill);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Bill::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $bill = Bill::query()
            ->select([
                'id',
                'order_id',
                'order_service_id',
                'total_price',
            ])
//            ->with('products')
            ->where('order_id', 'LIKE', "%$search%")
            ->orWhere('order_service_id', 'LIKE', "%$search%")
            ->orWhere('total_price', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($bill);
    }
}
