<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;
use App\Http\Requests\FloorCreateRequest;
use App\Floor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FloorController extends AbstractApiController
{
    public function getOption(Request $request)
    {
        $floor = Floor::query()->get();

        return $this->item($floor);
    }

    public function index(Request $request)
    {
        $floor = Floor::query()
            ->select([
                'id',
                'code_number_floor',
                'name'
            ])
            ->DataTablePaginate($request);

        return $this->item($floor);
    }

    public function create(FloorCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        $payload['code_number_floor']                   = $validatedData['code_number_floor'];
        $payload['name']                                = $validatedData['name'];

        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tầng');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $floor = Floor::create($payload);
        DB::beginTransaction();

        try {
            $floor->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm tầng thành công!');
            $this->setStatusCode(200);
            $this->setData($floor);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Floor::query()->findOrFail($id);
    }

    public function update(FloorCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $floor = Floor::query()->findOrFail($id);
        if (!$floor) {
            $this->setMessage('Không có tầng này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên tầng
                $floor->code_number_floor                   = $validatedData['code_number_floor'];
                $floor->name                                = $validatedData['name'];

                $floor->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($floor);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Floor::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $floor = Floor::query()->get();
        foreach ($floor->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $floor = Floor::query()
            ->select([
                'id',
                'code_number_floor',
                'name'
            ])
            ->where('code_number_floor', 'LIKE', "%$search%")
            ->orWhere('name', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($floor);
    }
}
