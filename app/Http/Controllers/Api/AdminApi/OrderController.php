<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\StaffCreateRequest;
use App\Order;
use App\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends AbstractApiController
{
    public function index(Request $request)
    {
        $order = Order::query()
            ->select([
                'id',
                'room_id',
                'contact_name',
                'sex',
                'description',
                'contact_mobile',
                'identity_number',
                'contact_address',
                'total_price',
                'prepay_price',
                'lack_price',
                'birthday',
                'order_date',
                'pay_date',
            ])
//            ->with('products')
            ->DataTablePaginate($request);

        return $this->item($order);
    }

    public function create(StaffCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        $payload['room_id']                                     = $validatedData['room_id'];
        $payload['contact_name']                                = $validatedData['contact_name'];
        $payload['sex']                                         = $validatedData['sex'];
        $payload['description']                                 = $validatedData['description'];
        $payload['contact_mobile']                              = $validatedData['contact_mobile'];
        $payload['identity_number']                             = $validatedData['identity_number'];
        $payload['contact_address']                             = $validatedData['contact_address'];
        $payload['total_price']                                 = $validatedData['total_price'];
        $payload['prepay_price']                                = $validatedData['prepay_price'];
        $payload['lack_price']                                  = $validatedData['lack_price'];
        $payload['birthday']                                    = $validatedData['birthday'];
        $payload['order_date']                                  = $validatedData['order_date'];
        $payload['pay_date']                                    = $validatedData['pay_date'];

        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['room_id'])) {
            $this->setMessage('Đã tồn tại đặt phòng');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $order = Order::create($payload);
        DB::beginTransaction();

        try {
            $order->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Đặt phòng thành công!');
            $this->setStatusCode(200);
            $this->setData($order);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
//        return Order::query()->with('products')->findOrFail($id);
        return Order::query()->findOrFail($id);
    }

    public function remove($id)
    {
        Order::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $order = Order::query()
            ->select([
                'id',
                'room_id',
                'contact_name',
                'sex',
                'description',
                'contact_mobile',
                'identity_number',
                'contact_address',
                'total_price',
                'prepay_price',
                'lack_price',
                'birthday',
                'order_date',
                'pay_date',
            ])
//            ->with('products')
            ->where('contact_name', 'LIKE', "%$search%")
            ->orWhere('contact_mobile', 'LIKE', "%$search%")
            ->orWhere('identity_number', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($order);
    }
}
