<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class OrderService extends Model
{
    use DataTablePaginate;

    protected $table = "order_services";
    protected $fillable = [
        'service_id',
        'bill_id',
        'order_count',
        'created_at',
    ];

    protected $filter = [
        'id',
        'service_id',
        'bill_id',
        'order_count',
        'created_at',
    ];

    public function services()
    {
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }
}
