<?php
namespace App\Support;
use Validator;

trait DataTablePaginate {
    public function scopeDataTablePaginate($query, $request) {
        $v = Validator::make($request->all(), [
            'page'              => 'required|min:1',
            'per_page'          => 'required|min:1',
            'sort_column'       => 'required|in:'.implode(',', $this->filter),
//            'direction'         => 'required|in:desc, esc',
        ]);

        if($v->fails()) {
            dd($v->messages());
        }
        return $query->orderBy($request->sort_column, $request->direction)
            ->paginate($request->per_page);
    }
}