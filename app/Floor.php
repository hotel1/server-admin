<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Support\DataTablePaginate;


class Floor extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'code_number_floor',
        'name'
    ];

    protected $filter = [
        'id',
        'code_number_floor',
        'name'
    ];
}
